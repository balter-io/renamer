#!/usr/bin/env python

import sys
import os
import shutil


def main():

    directory = sys.argv[1]
    chars = [('-', '_')]    # change characters to swap here (from, to)

    for root, dirs, files in os.walk(directory):
        for file in files:
            new_file = file

            for c in chars:
                new_file = new_file.replace(c[0], c[1])
            if new_file != file:
                temp_filename = new_file
                n = 0
                shutil.move(root+'/'+file, root+'/'+new_file)

    for root, dirs, files in os.walk(directory):
        for file in files:
            new_file = file

            for c in chars:
                new_file = new_file.replace(c[0], c[1])
            if new_file != file:
                temp_filename = new_file
                shutil.move(root+'/'+file, root+'/'+new_file)

        for index, folder in enumerate(dirs):
            new_dir = folder
            for c in chars:
                new_dir = new_dir.replace(c[0], c[1])
            if new_dir != folder:
                temp_dirname = new_dir
                shutil.move(root+'/'+folder, root+'/'+new_dir)
            if new_dir != folder:
                    dirs[index] = new_dir


if __name__ == '__main__':
    main()
