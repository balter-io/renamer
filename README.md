# Renamer

This is a renaming script in python 3. It takes a path as the first command line argument and can be modified to change
spaces to hyphens or underscores or any variation of the three.
